# IOC to control TDK Lambda Z series power supplies

## Used modules

*   [tdklambdaz](https://gitlab.esss.lu.se/e3/wrappers/ps/e3-tdklambdaz.git)


## Controlled devices

*   TS2-010CRM:Cryo-PSU-040
    *   TS2-010CRM:Cryo-PSU-041
    *   TS2-010CRM:Cryo-PSU-042
    *   TS2-010CRM:Cryo-PSU-043
    *   TS2-010CRM:Cryo-PSU-060
    *   TS2-010CRM:Cryo-PSU-061
