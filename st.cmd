#
# Module: essioc
#
require essioc

#
# Module: tdklambdaz
#
require tdklambdaz


#
# Setting STREAM_PROTOCOL_PATH
#
epicsEnvSet(STREAM_PROTOCOL_PATH, "${tdklambdaz_DB}")


#
# Module: essioc
#
iocshLoad("${essioc_DIR}/common_config.iocsh")

#
# Device: TS2-010CRM:Cryo-PSU-040
# Module: tdklambdaz
#
iocshLoad("${tdklambdaz_DIR}/tdklambdaz_master.iocsh", "DEVICENAME = TS2-010CRM:Cryo-PSU-040, IPADDR = ts2-cryo-tdklambda3.tn.esss.lu.se, RS485_ADDR = 1")

#
# Device: TS2-010CRM:Cryo-PSU-041
# Module: tdklambdaz
#
iocshLoad("${tdklambdaz_DIR}/tdklambdaz_slave.iocsh", "DEVICENAME = TS2-010CRM:Cryo-PSU-041, MASTER = TS2-010CRM:Cryo-PSU-040, RS485_ADDR = 2")

#
# Device: TS2-010CRM:Cryo-PSU-042
# Module: tdklambdaz
#
iocshLoad("${tdklambdaz_DIR}/tdklambdaz_slave.iocsh", "DEVICENAME = TS2-010CRM:Cryo-PSU-042, MASTER = TS2-010CRM:Cryo-PSU-040, RS485_ADDR = 3")

#
# Device: TS2-010CRM:Cryo-PSU-043
# Module: tdklambdaz
#
iocshLoad("${tdklambdaz_DIR}/tdklambdaz_slave.iocsh", "DEVICENAME = TS2-010CRM:Cryo-PSU-043, MASTER = TS2-010CRM:Cryo-PSU-040, RS485_ADDR = 4")

#
# Device: TS2-010CRM:Cryo-PSU-060
# Module: tdklambdaz
#
iocshLoad("${tdklambdaz_DIR}/tdklambdaz_slave.iocsh", "DEVICENAME = TS2-010CRM:Cryo-PSU-060, MASTER = TS2-010CRM:Cryo-PSU-040, RS485_ADDR = 5")

#
# Device: TS2-010CRM:Cryo-PSU-061
# Module: tdklambdaz
#
iocshLoad("${tdklambdaz_DIR}/tdklambdaz_slave.iocsh", "DEVICENAME = TS2-010CRM:Cryo-PSU-061, MASTER = TS2-010CRM:Cryo-PSU-040, RS485_ADDR = 6")
